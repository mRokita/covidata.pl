import datetime

from common import date_set
from local_utils import LocalCrawler

if __name__ == '__main__':
    days_to_fix = sorted(
        date_set(datetime.date(year=2020, month=11, day=24), datetime.date.today() + datetime.timedelta(days=1)))
    for d in days_to_fix:
        LocalCrawler(datetime.datetime.combine(d, datetime.datetime.min.time())).fix_daily_to_total()
