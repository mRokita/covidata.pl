from collections import Counter
import datetime
from datetime import timedelta
from json import loads
import re
import httpx
import csv
from html import unescape

from loguru import logger

from common import Crawler, ReportType, date_str, Client
from config import WAYBACK_SNAPSHOT_LIST_URL, PL_GOV_URL


class LocalCrawler(Crawler):
    report_type = ReportType.LOCAL
    reports_since = datetime.datetime(year=2020, month=3, day=9)
    enable_today = True

    @staticmethod
    def parse_int(num):
        """Parse int even if the string contains spaces"""
        return int(''.join(num.split()))

    def fetch(self):
        data = self.get_data()
        regions = dict()
        for row in data:

            name = row['Województwo'].lower()
            if not name:
                continue
            if name in ('cała polska', 'cały kraj'):
                name = 'Polska'
            total_cases = row.get(
                'Liczba', None) or row.get('Liczba przypadków')
            total_cases = self.parse_int(total_cases) if total_cases else 0

            total_deaths = row.get('Liczba zgonów', None) or row.get('Zgony', 0)
            total_deaths = self.parse_int(total_deaths) if total_deaths else 0

            row_counter = Counter({
                'total_cases': total_cases,
                'total_deaths': total_deaths
            })
            regions[name] = regions.get(name, Counter()) + row_counter
        for name, counter in regions.items():
            region_id = self.get_region_id(name)
            report_data = {
                'region_id': region_id,
                'cases': counter['total_cases'],
                'deaths': counter['total_deaths']
            }
            self.apply_recoveries(report_data)
            self.convert_daily_to_total(report_data)
            self.submit_day_report(**report_data)

        self.submit_download_success()

    def convert_daily_to_total(self, report_data):
        """
        Polish govt decided to report daily cases instead of total, that's a fix for that.
        """
        region_id = report_data['region_id']
        if not (self.date.date() > datetime.datetime(year=2020, month=11, day=23).date()):
            return
        with Client() as c:
            res = c.get(f"regions/{region_id}/day_reports/{date_str(self.date - timedelta(days=1))}")
        if res.status_code != 200:
            logger.error(f"Couldn't get the previous report for {self.date}, {region_id}")
            raise self.DataNotAvailable
        previous_report_data = res.json()
        if previous_report_data['total_cases'] > report_data['cases']:
            report_data['cases'] += previous_report_data['total_cases']
            report_data['deaths'] += previous_report_data['total_deaths']
        else:
            raise self.DataNotAvailable

    def apply_recoveries(self, report_data):
        """
        The official website providing data for poland doesn't store
         recoveries.
        Because of that, we should fill the new records with previous records.
        """
        region_id = report_data['region_id']
        with Client() as c:
            res = c.get(f"regions/{region_id}/day_reports/{date_str(self.date)}")
        if res.status_code != 200:
            return
        recoveries = res.json().get('total_recoveries', 0)
        if recoveries:
            return
        with Client() as c:
            res = c.get(
                f"regions/{region_id}"
                f"/day_reports"
                f"/{date_str(self.date - timedelta(days=1))}")
        if res.status_code != 200:
            return
        recoveries = res.json().get('total_recoveries', 0)
        report_data['recoveries'] = recoveries

    def submit_region(self, name: str) -> int:
        if name != 'Polska':
            return super().submit_region(name)
        else:
            return super()._submit_region(name, ReportType.GLOBAL)

    @staticmethod
    def parse_time(snap_timestamp: str) -> datetime:
        month = int(snap_timestamp[:2])
        day = int(snap_timestamp[2:4])
        hours = int(snap_timestamp[4:6])
        minutes = int(snap_timestamp[6:8])
        return datetime.datetime(2020, month, day, hours, minutes)

    def get_download_url(self) -> str:
        """
        Retrieve urls for web.archive.org snapshots of gov.pl cov site
        :return: Urls for gov.pl COV site
        """
        if self.is_today:
            return PL_GOV_URL

        if self.date.date() > datetime.date(year=2020, month=11, day=23):
            data = httpx.get('https://www.gov.pl/web/koronawirus/pliki-archiwalne-wojewodztwa')
            urls = re.findall(
                r'<a class="file-download" href="(/attachment/.*?)".*?>.*?(\d+)&#8203;_(\d+)&#8203;_(\d+).*?</a>',
                data.text,
                re.DOTALL)
            for url, day, month, year in urls:
                if int(month) == self.date.month and int(day) == self.date.day and int('20' + year) == self.date.year:
                    return f'https://www.gov.pl{url}'
            logger.error(f'No data for {self.date}, {urls}')

        with httpx.Client() as c:
            res = c.get(WAYBACK_SNAPSHOT_LIST_URL)

        snap_timestamps = map(  # That's just to parse stupid wayback's format
            lambda i: str(i[0]).zfill(10),
            reversed(res.json().get('items', []))
        )

        for timestamp in snap_timestamps:
            snap_time = self.parse_time(timestamp)
            if snap_time.date() == self.date.date():
                return (f'http://web.archive.org/web'
                        f'/2020{timestamp}id_/{PL_GOV_URL}')
        raise self.DataNotAvailable

    def get_data(self) -> dict:
        """
        Download data, get the tag with json stats,
        then strip html tags and return a dict containing stats
        :param url: Url of the gov.pl site containing coronavirus stats
        :return: A dict containing deserialized stats
        """
        url = self.get_download_url()
        tag = None
        if 'attachment' in url:
            data = []
            with httpx.Client() as c:
                res = c.get(url)
                res.encoding = 'windows-1250'
                headers = []
                for i, line in enumerate(res.text.split('\n')):
                    if i == 0:
                        headers = line.strip().split(';')
                    else:
                        data.append(dict(zip(headers, line.split(';'))))
            return data

        with httpx.Client() as c:
            res = c.get(url)
            for line in res.iter_lines():
                if 'id="registerData"' in line:
                    tag = line
                    break
        json = unescape(re.sub('<(.+?)>', '', tag))
        data = loads(loads(json)["parsedData"])
        return data
